$.FollowToggle = function (el, options) {
  this.$el = $(el);
  this.userId = this.$el.data("user-id") || options.userId;
  this.followState = this.$el.data("initial-follow-state") || options.followState;
  this.render();
  this.bindListeners();
};

$.FollowToggle.prototype.render = function () {
  this.$el.prop("disabled", false);
  if (this.followState === "followed") {
    this.$el.html("Unfollow");
  } else if (this.followState === "unfollowed") {
    this.$el.html("Follow");
  } else if (this.followState === "Following" || this.followState === "Unfollowing") {
    this.$el.html(this.followState);
    this.$el.prop("disabled", true);
  }
};

$.FollowToggle.prototype.handleClick = function (event) {
  event.preventDefault();
  var $target = $(event.currentTarget);
  var methodType = (this.followState === "followed") ? "DELETE" : "POST";
  this.followState = (this.followState === 'followed') ? 'Unfollowing' : 'Following';
  this.render();
  $.ajax({
    url: "/users/" + this.userId + "/follow",
    type: methodType,
    dataType: "json",
    success: function () {
      this.followState = (this.followState === 'Following') ? 'followed' : 'unfollowed';
      this.render();
    }.bind(this),
    error: function (req) {
      console.log(req);
    }
  });
};

$.FollowToggle.prototype.bindListeners = function () {
  this.$el.on('click', this.handleClick.bind(this));
};


$.FollowToggle.prototype.method1 = function () {
  // ...
};

$.fn.followToggle = function (options) {
  return this.each(function () {
    new $.FollowToggle(this, options);
  });
};

$(function () {
  $(".follow-toggle").followToggle();
});
