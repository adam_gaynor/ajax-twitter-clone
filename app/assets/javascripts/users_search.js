$.UsersSearch = function (el) {
  this.$el = $(el);
  this.$users = this.$el.find(".users");


  this.bindListeners();
};

$.UsersSearch.prototype.method1 = function () {
  // ...
};

$.UsersSearch.prototype.render = function (res) {
  res.forEach(function (user) {

    var $li = $("<li class=\"group\"></li>");
    var $anchor = $("<a href=\"" + user.id + "\">" + user.username + "</a>");
    var $button = $("<button class=\"follow-toggle\"></button>");

    $li.append($anchor);
    $li.append($button);

    var followState = user.followed === true ? "followed" : "unfollowed";
    var options = {"userId": user.id, "followState": followState };
    new $.FollowToggle($button[0], options);

    this.$users.append($li);

  }.bind(this))
};

$.UsersSearch.prototype.handleKeystroke = function (event) {
  event.preventDefault();
  var $target = $(event.currentTarget);
  var $input = $target.find("input");
  var val = $input.val().toLowerCase();
  $.ajax({
    url: "/users/search",
    type: "GET",
    dataType: "json",
    data: {"query": val},
    success: function (res) {
      this.$users.empty();
      console.log(res);
      this.render(res);
    }.bind(this),
    error: function (err) {
      console.log(err);
    }
  });
};


$.UsersSearch.prototype.bindListeners = function () {
  this.$el.on('keyup', this.handleKeystroke.bind(this));
};


$.fn.UsersSearch = function () {
  return this.each(function () {
    new $.UsersSearch(this);
  });
};

$(function () {
  $("section.users-search").UsersSearch();
});
